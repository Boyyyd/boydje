﻿using Boydje.Discord;
using Boydje.Discord.Entities;
using Discord.Net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Boydje.xUnit.Tests
{
    public class ConnectionTests
    {
        [Fact]
        public void ConnectionAsyncTest()
        {
            Assert.ThrowsAsync<HttpException>(AttemptWrongConnect);
        }

        private async Task AttemptWrongConnect()
        {
            var conn = Unity.Resolve<Connection>();
            await conn.ConnectAsync(new TutorialBotConfig { Token = "FAKE-TOKEN" });
        }
    }
}
