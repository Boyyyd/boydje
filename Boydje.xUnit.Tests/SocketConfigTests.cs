﻿using Boydje.Discord;
using Discord;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Boydje.xUnit.Tests
{
    public class SocketConfigTests
    {
        [Fact]
        public void ConfigDefaultTest()
        {
            const LogSeverity expected = LogSeverity.Verbose;
            var actual = SocketConfig.GetDefault().LogLevel;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ConfigNewTest()
        {
            var actual = SocketConfig.GetNew();

            Assert.NotNull(actual); 
        }
    }
}
