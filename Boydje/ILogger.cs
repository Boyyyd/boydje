﻿namespace Boydje
{
    public interface ILogger
    {
        void Log(string message);
    }
}
