﻿using Boydje.Discord;
using Boydje.Discord.Entities;
using Boydje.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Boydje
{
    public class Boyd
    {
        private readonly IDataStorage _storage;
        private readonly Connection _connection;

        public Boyd(IDataStorage storage, Connection connection)
        {
            _storage = storage;
            _connection = connection;
        }

        public async Task Start()
        {
            await _connection.ConnectAsync(new TutorialBotConfig()
            {
                Token = _storage.RestoreObject<string>("Config/BotToken")
            });
        }
    }
}
