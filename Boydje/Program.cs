﻿using Boydje.Discord;
using Boydje.Discord.Entities;
using Boydje.Storage;
using System;
using System.Threading.Tasks;

namespace Boydje
{
    internal class Program
    {
        private static async Task Main()
        {
            var bot = Unity.Resolve<Boyd>();
            await bot.Start();
        }
    }
}